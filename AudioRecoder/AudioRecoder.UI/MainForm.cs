﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Linq;
using AudioRecoder.UI.Code;
using NAudio.Wave;

namespace AudioRecoder.UI
{
    public partial class MainForm : Form
    {
        private bool _isConnected = false;
        //сокет отправитель
        private readonly Socket _client;
        //поток для нашей речи
        private WaveIn _input;
        private WaveFileWriter _writer;
        private WaveFileWriter _oldWriter;
        private readonly string _configFileName;

        private bool _autoRecordingEnabled = false;
        private SettingManager _settingManager;
        private Settings _settings;

        private string _recordPath;
        private string _recordFileName;
        private string _toBeRenamed;
        private string _oldRecordFileName;

        private WaveIn _fileRecoder;
        private WaveIn _oldFileRecoder;
        private bool _isRestarted = false;
        private object _sync = new object();

        private FileSplitEvent _fileSplitEvent;

        public string RootPath
        {
            get
            {
                return XDocument.Load(_configFileName).Root.Elements().Where(el => el.Name == "SavePath").First().Value;
            }
        }

        public MainForm()
        {
            InitializeComponent();

            _configFileName = System.Configuration.ConfigurationManager.AppSettings["ConfigFileName"].ToString();
            _settingManager = new SettingManager(_configFileName);

            _settings = _settingManager.LoadSettings();
            _autoRecordingEnabled = _settings.AutoRecordingEnabled;
            btnStartRecordInFile.Enabled = !_autoRecordingEnabled;
            btnStopRecordInFile.Enabled = _autoRecordingEnabled;
            txbIpAddr.Text = _settings.IpAddress;

            //создаем поток для записи нашей речи
            _input = new WaveIn();
            //определяем его формат - частота дискретизации 8000 Гц, ширина сэмпла - 16 бит, 1 канал - моно
            _input.WaveFormat = new WaveFormat(8000, 16, 1);
            //добавляем код обработки нашего голоса, поступающего на микрофон
            _input.DataAvailable += DataAvaliableForRemoteTransfer;

            /*_fileRecoder = new WaveIn();
            _fileRecoder.WaveFormat = new WaveFormat(8000, 1);
            _fileRecoder.DataAvailable += DataAvailableForRecordInFile;
            _fileRecoder.RecordingStopped += RecordingStoppedForRecordInFile;

            if (_autoRecordingEnabled)
            {
                CreateRecordFolder();
                SetRecordFileName();
                _writer = new WaveFileWriter(GetRecordFilePath(_recordFileName), _fileRecoder.WaveFormat);
                _fileRecoder.StartRecording();
            }*/

            if (_autoRecordingEnabled)
            {
                StartRecordingInFile();
            }

            ////сокет для отправки звука
            _client = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            _isConnected = true;
        }

        #region Voice Event Handlers

        private void DataAvailableForRecordInFile(object sender, WaveInEventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new EventHandler<WaveInEventArgs>(DataAvailableForRecordInFile), sender, e);
            }
            else
            {
                _writer.WriteData(e.Buffer, 0, e.BytesRecorded);
            }
        }

        private void RecordingStoppedForRecordInFile(object sender, StoppedEventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new EventHandler<StoppedEventArgs>(RecordingStoppedForRecordInFile), sender, e);
            }
            else
            {
                Debug.WriteLine("IN RecordingStoppedForRecordInFile");
                if (_oldFileRecoder != null)
                {
                    _oldFileRecoder.Dispose();
                    _oldFileRecoder = null;
                }
                if (_oldWriter != null)
                {
                    _oldWriter.Close();
                    _oldWriter.Dispose();
                    _oldWriter = null;
                    RenameFile(_toBeRenamed);
                }

                lock (_sync)
                {
                    if (_isRestarted)
                    {
                        _isRestarted = false;
                    }
                }
            }
        }


        //Обработка нашего голоса
        private void DataAvaliableForRemoteTransfer(object sender, WaveInEventArgs e)
        {
            try
            {
                //Подключаемся к удаленному адресу
                var remotePoint = new IPEndPoint(IPAddress.Parse(_settings.IpAddress), _settings.Port);

                //посылаем байты, полученные с микрофона на удаленный адрес
                _client.SendTo(e.Buffer, remotePoint);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #endregion

        #region Event Handlers

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            _isConnected = false;

            if (_input != null)
            {
                _input.Dispose();
                _input = null;
            }

            if (_fileRecoder != null)
            {
                StopRecordingInFile();
            }

            _client.Close();
            _client.Dispose();
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var of = new OptionsForm();

            of.ShowDialog(this);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            _input.StartRecording();
        }

        private void btnStartRecordInFile_Click(object sender, EventArgs e)
        {
            _autoRecordingEnabled = true;
            btnStartRecordInFile.Enabled = false;
            btnStopRecordInFile.Enabled = true;

            StartRecordingInFile();
        }

        private void btnStopRecordInFile_Click(object sender, EventArgs e)
        {
            _autoRecordingEnabled = false;
            btnStartRecordInFile.Enabled = true;
            btnStopRecordInFile.Enabled = false;

            StopRecordingInFile();
        }

        #endregion

        private void CreateRecordFolder()
        {
            var settings = _settingManager.LoadSettings();

            var path = settings.RootPath + "\\";
            path += DateTime.Now.Date.ToString("dd-MM-yyyy");
            _recordPath = path;
            Directory.CreateDirectory(path);
        }

        private void SetRecordFileName()
        {
            _recordFileName = String.Format("{0};{1};{2}-", DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
            _recordFileName += "{0}.waw";
        }

        private string GetRecordFilePath(string recordFileName)
        {
            return _recordPath + "\\" + recordFileName;
        }

        private void RenameFile(string toBeRenamed)
        {
            File.Move(GetRecordFilePath(_oldRecordFileName), GetRecordFilePath(toBeRenamed));
        }

        private void RestartRecord(object sender, EventArgs e)
        {
            StopRecordingInFile();

            StartRecordingInFile();
        }

        private void StartRecordingInFile()
        {
            _fileRecoder = new WaveIn();
            _fileRecoder.WaveFormat = new WaveFormat(8000, 1);
            _fileRecoder.DataAvailable += DataAvailableForRecordInFile;
            _fileRecoder.RecordingStopped += RecordingStoppedForRecordInFile;

            if (_autoRecordingEnabled)
            {
                CreateRecordFolder();
                SetRecordFileName();
                _writer = new WaveFileWriter(GetRecordFilePath(_recordFileName), _fileRecoder.WaveFormat);
                _fileRecoder.StartRecording();
                _fileSplitEvent = new FileSplitEvent(GetRecordFilePath(_recordFileName), _settings.SplitFileSizeBytes);
                _fileSplitEvent.OnFileSplitted += RestartRecord;
            }
        }

        private void StopRecordingInFile()
        {
            _oldWriter = _writer;
            _oldFileRecoder = _fileRecoder;
            _toBeRenamed = String.Format(_recordFileName,
                    String.Format("{0};{1};{2}", DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second));
            _oldRecordFileName = _recordFileName;

            if (_oldFileRecoder != null)
            {
                _oldFileRecoder.StopRecording();
            }
        }
    }
}
