﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using AudioRecoder.UI.Code;

namespace AudioRecoder.UI
{
    public partial class OptionsForm : Form
    {
        private readonly string _configFileName;
        private SettingManager _settingManager;

        public OptionsForm()
        {
            InitializeComponent();

            _configFileName = System.Configuration.ConfigurationManager.AppSettings["ConfigFileName"].ToString();
            _settingManager = new SettingManager(_configFileName);

            var settings = _settingManager.LoadSettings();

            cxbAutoRecordingEnabled.Checked = settings.AutoRecordingEnabled;
            cxbAutoConnectionEnabled.Checked = settings.AutoConnectionEnabled;
            txbIpAddr.Text = settings.IpAddress;
            txbRootPath.Text = settings.RootPath;
            txbSplitFileSize.Text = settings.SplitFileSize.ToString();
        }

        #region Event Handlers

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var settings = TranslateSettings();
            SaveSettings(settings);

            Owner.Close();
        }

        private void btnChangeRootPath_Click(object sender, EventArgs e)
        {
            var fbd = new FolderBrowserDialog();

            if (fbd.ShowDialog() != DialogResult.Cancel)
            {
                txbRootPath.Text = fbd.SelectedPath;
            }
        }


        #endregion


        #region Private Helpers

        private Settings TranslateSettings()
        {
            var settings = _settingManager.LoadSettings();

            settings.AutoRecordingEnabled = cxbAutoRecordingEnabled.Checked;
            settings.AutoConnectionEnabled = cxbAutoConnectionEnabled.Checked;
            settings.IpAddress = txbIpAddr.Text;
            settings.RootPath = txbRootPath.Text;
            settings.SplitFileSize = Int32.Parse(txbSplitFileSize.Text);

            return settings;
        }

        private void SaveSettings(Settings settings)
        {
            _settingManager.SaveSettings(settings);
        }

        #endregion

    }
}
