﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AudioRecoder.UI.Code
{
    public class Settings
    {
        public bool AutoRecordingEnabled { get; set; }
        public bool AutoConnectionEnabled { get; set; }
        public string RootPath { get; set; } 
        public string IpAddress { get; set; }
        public int Port { get; set; }
        public int SplitFileSize { get; set; }

        public long SplitFileSizeBytes
        {
            get
            {
                return SplitFileSize*1024*1024;
                /*return 1024*300;*/
            }
        }
    }
}
