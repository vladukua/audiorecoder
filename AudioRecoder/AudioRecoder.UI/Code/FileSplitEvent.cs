﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NAudio.Wave;

namespace AudioRecoder.UI.Code
{
    public class FileSplitEvent
    {
        public event EventHandler OnFileSplitted;
        private Timer _timer;
        private long _maxLength;
        private FileInfo _fileInfo;

        public FileSplitEvent(string filePath, long maxLength)
        {
            _fileInfo = new FileInfo(filePath);
            _timer = new Timer();

            _timer.Interval = 1000;
            _timer.Tick += CheckLength;
            _maxLength = maxLength;
            _timer.Start();
        }

        private void CheckLength(object sender, EventArgs e)
        {
            try
            {
                _fileInfo.Refresh();
                if (_fileInfo.Length > _maxLength)
                {
                    if (OnFileSplitted != null)
                    {
                        _timer.Stop();
                        OnFileSplitted(this, EventArgs.Empty);
                    }
                }
            }
            catch (Exception)
            {
                _timer.Stop();
            }

        }
    }
}
