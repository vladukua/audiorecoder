﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace AudioRecoder.UI.Code
{
    public class SettingManager
    {
        private string _configFilePath;

        public SettingManager(string configFilePath)
        {
            _configFilePath = configFilePath;
        }

        public void SaveSettings(Settings settings)
        {
            var doc = XDocument.Load(_configFilePath);

            var element = doc.Root.Elements().Where(el => el.Name == "AutoRecordingEnabled").First();
            element.Value = settings.AutoRecordingEnabled.ToString();

            element = doc.Root.Elements().Where(el => el.Name == "AutoConnectionEnabled").First();
            element.Value = settings.AutoConnectionEnabled.ToString();

            if (settings.RootPath != null)
            {
                element = doc.Root.Elements().Where(el => el.Name == "SavePath").First();
                element.Value = settings.RootPath;
            }

            if (settings.IpAddress != null)
            {
                element = doc.Root.Elements().Where(el => el.Name == "RemoteIpAddr").First();
                element.Value = settings.IpAddress;
            }

            element = doc.Root.Elements().Where(el => el.Name == "SplitFileSize").First();
            element.Value = settings.SplitFileSize.ToString();

            element = doc.Root.Elements().Where(el => el.Name == "Port").First();
            element.Value = settings.Port.ToString();

            doc.Save(_configFilePath);
        }

        public Settings LoadSettings()
        {
            var settings = new Settings();
            var doc = XDocument.Load(_configFilePath);

            settings.AutoRecordingEnabled = Boolean.Parse(doc.Root.Elements().Where(el => el.Name == "AutoRecordingEnabled").First().Value);

            settings.RootPath = doc.Root.Elements().Where(el => el.Name == "SavePath").First().Value;

            settings.AutoConnectionEnabled = Boolean.Parse(doc.Root.Elements().Where(el => el.Name == "AutoConnectionEnabled").First().Value);

            settings.IpAddress = doc.Root.Elements().Where(el => el.Name == "RemoteIpAddr").First().Value;

            settings.SplitFileSize = Int32.Parse(doc.Root.Elements().Where(el => el.Name == "SplitFileSize").First().Value);

            settings.Port = Int32.Parse(doc.Root.Elements().Where(el => el.Name == "Port").First().Value);

            return settings;
        }
    }
}
