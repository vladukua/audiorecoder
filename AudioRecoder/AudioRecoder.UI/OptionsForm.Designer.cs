﻿namespace AudioRecoder.UI
{
    partial class OptionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cxbAutoRecordingEnabled = new System.Windows.Forms.CheckBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.cxbAutoConnectionEnabled = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txbIpAddr = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txbRootPath = new System.Windows.Forms.TextBox();
            this.btnChangeRootPath = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txbSplitFileSize = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cxbAutoRecordingEnabled
            // 
            this.cxbAutoRecordingEnabled.AutoSize = true;
            this.cxbAutoRecordingEnabled.Location = new System.Drawing.Point(10, 9);
            this.cxbAutoRecordingEnabled.Name = "cxbAutoRecordingEnabled";
            this.cxbAutoRecordingEnabled.Size = new System.Drawing.Size(83, 17);
            this.cxbAutoRecordingEnabled.TabIndex = 0;
            this.cxbAutoRecordingEnabled.Text = "Авто запис";
            this.cxbAutoRecordingEnabled.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(123, 225);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Зберегти";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(227, 225);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Відміна";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // cxbAutoConnectionEnabled
            // 
            this.cxbAutoConnectionEnabled.AutoSize = true;
            this.cxbAutoConnectionEnabled.Location = new System.Drawing.Point(10, 34);
            this.cxbAutoConnectionEnabled.Name = "cxbAutoConnectionEnabled";
            this.cxbAutoConnectionEnabled.Size = new System.Drawing.Size(103, 17);
            this.cxbAutoConnectionEnabled.TabIndex = 3;
            this.cxbAutoConnectionEnabled.Text = "Авто з\'єднання";
            this.cxbAutoConnectionEnabled.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(76, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "IP Address";
            // 
            // txbIpAddr
            // 
            this.txbIpAddr.Location = new System.Drawing.Point(156, 72);
            this.txbIpAddr.Name = "txbIpAddr";
            this.txbIpAddr.Size = new System.Drawing.Size(184, 20);
            this.txbIpAddr.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(44, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Кореневий шлях";
            // 
            // txbRootPath
            // 
            this.txbRootPath.Location = new System.Drawing.Point(156, 108);
            this.txbRootPath.Name = "txbRootPath";
            this.txbRootPath.ReadOnly = true;
            this.txbRootPath.Size = new System.Drawing.Size(184, 20);
            this.txbRootPath.TabIndex = 7;
            // 
            // btnChangeRootPath
            // 
            this.btnChangeRootPath.Location = new System.Drawing.Point(356, 107);
            this.btnChangeRootPath.Name = "btnChangeRootPath";
            this.btnChangeRootPath.Size = new System.Drawing.Size(59, 23);
            this.btnChangeRootPath.TabIndex = 8;
            this.btnChangeRootPath.Text = "Змінити";
            this.btnChangeRootPath.UseVisualStyleBackColor = true;
            this.btnChangeRootPath.Click += new System.EventHandler(this.btnChangeRootPath_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 135);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(131, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Розмір файлу для поділу";
            // 
            // txbSplitFileSize
            // 
            this.txbSplitFileSize.Location = new System.Drawing.Point(156, 143);
            this.txbSplitFileSize.Name = "txbSplitFileSize";
            this.txbSplitFileSize.Size = new System.Drawing.Size(64, 20);
            this.txbSplitFileSize.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(223, 147);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "МБ";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.cxbAutoConnectionEnabled);
            this.panel1.Controls.Add(this.cxbAutoRecordingEnabled);
            this.panel1.Location = new System.Drawing.Point(2, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(148, 177);
            this.panel1.TabIndex = 12;
            // 
            // OptionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(425, 286);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txbSplitFileSize);
            this.Controls.Add(this.btnChangeRootPath);
            this.Controls.Add(this.txbRootPath);
            this.Controls.Add(this.txbIpAddr);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "OptionsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "AudioRecoder Settings";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox cxbAutoRecordingEnabled;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.CheckBox cxbAutoConnectionEnabled;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txbIpAddr;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txbRootPath;
        private System.Windows.Forms.Button btnChangeRootPath;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txbSplitFileSize;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel1;
    }
}