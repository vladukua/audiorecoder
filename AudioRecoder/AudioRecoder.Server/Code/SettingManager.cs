﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace AudioRecoder.Server.Code
{
    public class SettingManager
    {
        private string _configPath;

        public SettingManager(string configPath)
        {
            _configPath = configPath;
        }

        public Settings LoadSettings()
        {
            var settings = new Settings();

            var doc = XDocument.Load(_configPath);

            settings.IpAddress = doc.Root.Elements().Where(el => el.Name == "HostIpAddr").First().Value;

            settings.Port = Int32.Parse(doc.Root.Elements().Where(el => el.Name == "Port").First().Value);

            return settings;
        }

        public void SaveSettings(Settings settings)
        {
            var doc = XDocument.Load(_configPath);
            XElement element;

            if (settings.IpAddress != null)
            {
                element = doc.Root.Elements().Where(el => el.Name == "HostIpAddr").First();
                element.Value = settings.IpAddress;
            }

            element = doc.Root.Elements().Where(el => el.Name == "Port").First();
            element.Value = settings.Port.ToString();

            doc.Save(_configPath);
        }
    }
}
