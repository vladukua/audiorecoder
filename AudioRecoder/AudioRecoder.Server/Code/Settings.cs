﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AudioRecoder.Server.Code
{
    public class Settings
    {
        public string IpAddress { get; set; }
        public int Port { get; set; }
    }
}
