﻿namespace AudioRecoder.Server
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txbIpAddr = new System.Windows.Forms.TextBox();
            this.btnStartListen = new System.Windows.Forms.Button();
            this.btnCancelListen = new System.Windows.Forms.Button();
            this.btnChangeIpAddr = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txbIpAddr
            // 
            this.txbIpAddr.Location = new System.Drawing.Point(12, 67);
            this.txbIpAddr.Name = "txbIpAddr";
            this.txbIpAddr.Size = new System.Drawing.Size(165, 20);
            this.txbIpAddr.TabIndex = 0;
            this.txbIpAddr.Text = "192.168.0.33";
            // 
            // btnStartListen
            // 
            this.btnStartListen.Location = new System.Drawing.Point(57, 176);
            this.btnStartListen.Name = "btnStartListen";
            this.btnStartListen.Size = new System.Drawing.Size(75, 23);
            this.btnStartListen.TabIndex = 1;
            this.btnStartListen.Text = "Слухати";
            this.btnStartListen.UseVisualStyleBackColor = true;
            this.btnStartListen.Click += new System.EventHandler(this.btnStartListen_Click);
            // 
            // btnCancelListen
            // 
            this.btnCancelListen.Location = new System.Drawing.Point(153, 176);
            this.btnCancelListen.Name = "btnCancelListen";
            this.btnCancelListen.Size = new System.Drawing.Size(75, 23);
            this.btnCancelListen.TabIndex = 2;
            this.btnCancelListen.Text = "Зупинити";
            this.btnCancelListen.UseVisualStyleBackColor = true;
            this.btnCancelListen.Click += new System.EventHandler(this.btnCancelListen_Click);
            // 
            // btnChangeIpAddr
            // 
            this.btnChangeIpAddr.Location = new System.Drawing.Point(195, 66);
            this.btnChangeIpAddr.Name = "btnChangeIpAddr";
            this.btnChangeIpAddr.Size = new System.Drawing.Size(75, 23);
            this.btnChangeIpAddr.TabIndex = 3;
            this.btnChangeIpAddr.Text = "Змінити";
            this.btnChangeIpAddr.UseVisualStyleBackColor = true;
            this.btnChangeIpAddr.Click += new System.EventHandler(this.btnChangeIpAddr_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 250);
            this.Controls.Add(this.btnChangeIpAddr);
            this.Controls.Add(this.btnCancelListen);
            this.Controls.Add(this.btnStartListen);
            this.Controls.Add(this.txbIpAddr);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Audio Recoder (Server)";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txbIpAddr;
        private System.Windows.Forms.Button btnStartListen;
        private System.Windows.Forms.Button btnCancelListen;
        private System.Windows.Forms.Button btnChangeIpAddr;
    }
}

