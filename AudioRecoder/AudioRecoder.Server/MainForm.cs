﻿using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;
using AudioRecoder.Server.Code;
using NAudio.Wave;

namespace AudioRecoder.Server
{
    public partial class MainForm : Form
    {
        private bool _isConnected = false;
        WaveOut _outputStream;
        //буфферный поток для передачи через сеть
        BufferedWaveProvider _bufferStream;
        //поток для прослушивания входящих сообщений
        Thread in_thread;
        //сокет для приема (протокол UDP)
        Socket listeningSocket;
        private IPEndPoint _localIP;
        private SettingManager _settingManager;
        private Settings _settings;

        private bool _startListenBtnEnabled = true;

        public MainForm()
        {
            InitializeComponent();

            btnStartListen.Enabled = _startListenBtnEnabled;
            btnCancelListen.Enabled = !_startListenBtnEnabled;
            btnChangeIpAddr.Enabled = _startListenBtnEnabled;

            var configFileName = System.Configuration.ConfigurationManager.AppSettings["ConfigPath"].ToString();
            _settingManager = new SettingManager(configFileName);

            _settings = _settingManager.LoadSettings();

            txbIpAddr.Text = _settings.IpAddress;
        }

        //Прослушивание входящих подключений
        private void Listening()
        {
            //Прослушиваем по адресу
            _localIP = new IPEndPoint(IPAddress.Parse(_settings.IpAddress), _settings.Port);
            var listeningSocketCopy = listeningSocket;
            listeningSocketCopy.Bind(_localIP);
            //начинаем воспроизводить входящий звук
            _outputStream.Play();
            //адрес, с которого пришли данные
            EndPoint remoteIp = new IPEndPoint(IPAddress.Any, 0);
            //бесконечный цикл
            while (_isConnected == true)
            {
                try
                {
                    //промежуточный буфер
                    byte[] data = new byte[65535];
                    //получено данных
                    int received = listeningSocketCopy.ReceiveFrom(data, ref remoteIp);
                    //добавляем данные в буфер, откуда output будет воспроизводить звук
                    _bufferStream.AddSamples(data, 0, received);
                }
                catch (SocketException ex)
                {

                }
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            _isConnected = false;

            if (listeningSocket != null)
            {
                listeningSocket.Close();
                listeningSocket.Dispose();
            }

            if (_outputStream != null)
            {
                _outputStream.Stop();
                _outputStream.Dispose();
                _outputStream = null;
            }

            _bufferStream = null;
        }

        private void btnStartListen_Click(object sender, System.EventArgs e)
        {
            _startListenBtnEnabled = false;
            btnStartListen.Enabled = _startListenBtnEnabled;
            btnCancelListen.Enabled = !_startListenBtnEnabled;
            btnChangeIpAddr.Enabled = _startListenBtnEnabled;

            //создаем поток для прослушивания входящего звука
            _outputStream = new WaveOut();
            //создаем поток для буферного потока и определяем у него такой же формат как и потока с микрофона
            _bufferStream = new BufferedWaveProvider(new WaveFormat(8000, 16, 1));
            //привязываем поток входящего звука к буферному потоку
            _outputStream.Init(_bufferStream);
            _isConnected = true;
            listeningSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            //создаем поток для прослушивания
            in_thread = new Thread(new ThreadStart(Listening));
            //запускаем его
            in_thread.Start();
        }

        private void btnCancelListen_Click(object sender, System.EventArgs e)
        {
            _startListenBtnEnabled = true;
            btnStartListen.Enabled = _startListenBtnEnabled;
            btnCancelListen.Enabled = !_startListenBtnEnabled;
            btnChangeIpAddr.Enabled = _startListenBtnEnabled;

            _isConnected = false;
            in_thread.Abort();
            listeningSocket.Close();
            listeningSocket.Dispose();

            Thread.Sleep(100);
        }

        private void btnChangeIpAddr_Click(object sender, System.EventArgs e)
        {
            _settings.IpAddress = txbIpAddr.Text;

            _settingManager.SaveSettings(_settings);
        }
    }
}
